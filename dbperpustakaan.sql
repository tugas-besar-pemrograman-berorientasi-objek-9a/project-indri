-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 03 Des 2019 pada 02.19
-- Versi server: 10.1.36-MariaDB
-- Versi PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbperpustakaan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `NamaUser` varchar(20) NOT NULL,
  `NIP` int(20) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `PasswordUser` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`NamaUser`, `NIP`, `Username`, `PasswordUser`) VALUES
('admin', 12, 'admiin', 'admin'),
('buday', 888, 'budi', 'budii');

-- --------------------------------------------------------

--
-- Struktur dari tabel `anggota`
--

CREATE TABLE `anggota` (
  `NamaAnggota` varchar(20) NOT NULL,
  `NIM` varchar(20) NOT NULL,
  `UsernameAnggota` varchar(20) NOT NULL,
  `PasswordAnggota` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `anggota`
--

INSERT INTO `anggota` (`NamaAnggota`, `NIM`, `UsernameAnggota`, `PasswordAnggota`) VALUES
('dimas', 'F1D016077', 'dimas', 'dimas'),
('ica', 'F1D017033', 'ica', 'ica'),
('elsa', 'F1D017083', 'elsa', 'elsa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `buku`
--

CREATE TABLE `buku` (
  `KodeBuku` int(15) NOT NULL,
  `JudulBuku` varchar(30) NOT NULL,
  `Pengarang` varchar(20) NOT NULL,
  `penerbit` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `buku`
--

INSERT INTO `buku` (`KodeBuku`, `JudulBuku`, `Pengarang`, `penerbit`) VALUES
(2, 'Ashiap', 'Atta', 'Halilintar'),
(3, 'Ashiap2', 'S', 'S'),
(4, 'Aa', 'AAaass', 'ss'),
(33, 'algoritma', 'dede', 'kaka');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

CREATE TABLE `peminjaman` (
  `KodeBuku` int(15) NOT NULL,
  `JudulBuku` varchar(30) NOT NULL,
  `NIM` varchar(20) NOT NULL,
  `TglPinjam` date NOT NULL,
  `TglKembali` date NOT NULL,
  `Status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`KodeBuku`, `JudulBuku`, `NIM`, `TglPinjam`, `TglKembali`, `Status`) VALUES
(2, 'Ashiap', 'F1D016076', '2019-12-01', '2019-12-05', '');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`NIM`);

--
-- Indeks untuk tabel `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`KodeBuku`);

--
-- Indeks untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`KodeBuku`),
  ADD UNIQUE KEY `NIM` (`NIM`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD CONSTRAINT `peminjaman_ibfk_1` FOREIGN KEY (`KodeBuku`) REFERENCES `buku` (`KodeBuku`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
