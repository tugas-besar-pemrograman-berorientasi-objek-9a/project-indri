/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplikasihperpustakaan;

import java.sql.DriverManager;
import com.mysql.jdbc.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author ASUS
 */
public class Koneksi {
    private static Connection conn;

    public static Connection koneksiDB(){
       
            try {
                 String DB="jdbc:mysql://localhost/dbperpustakaan";
                 String user="root";
                 String pass="";
                 Class.forName("com.mysql.jdbc.Driver");
                 conn=(Connection)DriverManager.getConnection(DB,user,pass);
             } catch (ClassNotFoundException ex) {
                 Logger.getLogger(Koneksi.class.getName()).log(Level.SEVERE, null, ex);
             } catch (SQLException ex) {}
          
         return conn;
     }
    
}
